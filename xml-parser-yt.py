#!/usr/bin/python3

#
# Simple XML parser for JokesXML
# Jesus M. Gonzalez-Barahona
# jgb @ gsyc.es
# SARO and SAT subjects (Universidad Rey Juan Carlos)
# 2009-2020
#
# Just prints the jokes in a JokesXML file

from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import urllib.request

PAGE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <h1>Contenidos del canal: </h1>
    <ul>
{videos}
    </ul>
  </body>
</html>
"""


class CounterHandler(ContentHandler):

    def __init__ (self):
        self.inEntry = False
        self.inVideo = False
        self.title = ""
        self.theVideo = ""
        self.url = ""
        self.videos = ""

    def startElement (self, name, attrs):
        if name == 'entry':
            self.inEntry = True
        elif self.inEntry:
            if name == 'title':
                self.inVideo = True
            elif name == 'link':
                self.url = attrs.get('href')
            
    def endElement (self, name):
        if name == 'entry':
            self.inEntry = False
            self.videos = self.videos \
                     + "    <li><a href='" + self.url + "'>" \
                     + self.title + "</a></li>\n"

        elif self.inEntry:
            if name == 'title':
                self.title = self.theVideo
                self.theVideo = ""
                self.inVideo = False
            
        
    def characters (self, chars):
        if self.inVideo:
            self.theVideo = self.theVideo + chars
            
# --- Main prog
if __name__ == "__main__":
    
    
    # Load parser and driver
    YtParser = make_parser()
    YtHandler = CounterHandler()
    YtParser.setContentHandler(YtHandler)

    # Ready, set, go!
    xmlFile = urllib.request.urlopen("https://www.youtube.com/feeds/videos.xml?channel_id=UC300utwSVAYOoRLEqmsprfg")
    YtParser.parse(xmlFile)

    print("Parse complete")
    print(PAGE.format(videos=YtHandler.videos))